

#include <intrinsics.h>
#include <stdlib.h>
#include <string.h>

#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_pwm.h"
#include "stm8_motor.h"

void interval_function(void);
void uart_receiver(char * message);

void main( void )
{
    InitialiseSystemClock();
    stm8_gpio_init(); 
    stm8_gpio_set_led(ON);
    
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*        STM8_AUTO_ROBOT        *\n");
    stm8_uart_printf("*********************************\n");
    stm8_uart_set_callback(uart_receiver);
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
    stm8_timer_set_interval(interval_function, 20 /*ms*/);
    stm8_pwm_init();
    __enable_interrupt();
    while(1)
    {
        
    }
}

void uart_receiver(char * message)
{
    if (strstr(message, "speed "))
    {
        stm8_uart_printf("speed: ");
        int speed = atoi(message + sizeof("speed: ") - 2);
        stm8_uart_print_int(speed);
        stm8_uart_printf("\n");
        stm8_motor_speed(speed);
    }
    if (strstr(message, "left_right "))
    {
        stm8_uart_printf("lr: ");
        const char s[2] = " ";
        char *token;
        token = strtok(message, s);
        token = strtok(NULL, s);
        
        int left = atoi(token);
        stm8_uart_print_int(left);
        stm8_motor_left(left);
        
        token = strtok(NULL, s);
        int right = atoi(token);
        stm8_uart_print_int(right);
        stm8_motor_right(right);
        stm8_uart_printf("\n");
    }
}

uint16_t counter = 0;
void interval_function(void)
{
    if (counter++ == 25)
    {
      stm8_gpio_toggle_led();
        counter = 0;
    }
}