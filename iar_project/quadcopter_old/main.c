/* main.c */
#include <intrinsics.h>
#include <intrinsics.h>
#include <math.h>

#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_pwm.h"
#include "stm8_sensor.h"
#include "stm8_ir.h"
#include "stm8_angle.h"
#include "stm8_pid.h"

void interval_function(void);
void ir_receiver(char * data);
void print_sensor_data(sensor_data_t sensor);

void main( void )
{
    InitialiseSystemClock();
    stm8_gpio_init();
    stm8_gpio_set_led(ON);
    stm8_gpio_exti_set_callback(stm8_ir_signal_analysis);
    stm8_ir_set_callback(ir_receiver);
    
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*        STM8_QUADCOPTER        *\n");
    stm8_uart_printf("*********************************\n");
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
    stm8_timer_set_interval(interval_function, 20 /*ms*/);
    stm8_pwm_init();
    __enable_interrupt();
    stm8_sensor_init();
    uint16_t counter;
    counter = 0;
    while(1)
    {
        stm8_timer_delay(500);
        stm8_gpio_toggle_led();
        stm8_pwm_set_value(STM8_PWM1|STM8_PWM2|STM8_PWM3|STM8_PWM4,counter*50);
        counter = (counter + 1)%10;
    }
}

stm8_angle_t gAngleX;

void interval_function(void)
{
    sensor_data_t sensor;
    sensor = stm8_sensor_get_value();
    float angleX = stm8_angle_get_value(&gAngleX, sensor.gyro_z, (int)sensor.accel_z, (int)sensor.accel_x,0.02);
    stm8_uart_print_float(angleX);
    print_sensor_data(sensor);
    stm8_uart_printf("\n");
    // pid_update_pwm();
}
void ir_receiver(char * data)
{
    stm8_uart_printf(data);
    stm8_uart_printf(" -- ");
    stm8_uart_print_int(data[0]);
    stm8_uart_printf("\n");
    
}

void print_sensor_data(sensor_data_t sensor)
{
    stm8_uart_printf("    Accel: ");
    stm8_uart_print_int(sensor.accel_x);
    stm8_uart_printf(", ");
    stm8_uart_print_int(sensor.accel_y);
    stm8_uart_printf(", ");
    stm8_uart_print_int(sensor.accel_z);
    
    stm8_uart_printf("   Gyro: ");
    stm8_uart_print_int(sensor.gyro_x);
    stm8_uart_printf(", ");         
    stm8_uart_print_int(sensor.gyro_y);
    stm8_uart_printf(", ");         
    stm8_uart_print_int(sensor.gyro_z);
}
