/* fly_cam.c */
#include <intrinsics.h>
#include <intrinsics.h>
#include <math.h>
#include <string.h>

// #define STM8_PWM_MAX 300

#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_pwm.h"
#include "stm8_sensor.h"
#include "stm8_angle.h"
#include "stm8_adc.h"
#include "stm8_pid.h"
#include "stm8_sonic.h"

void interval_function(void);
void print_sensor_data(sensor_data_t sensor);

float voltage;
extern uint32_t flyingTime;
extern float kDistP;
extern float kDistI;
extern float kDistD;

void main( void )
{
    InitialiseSystemClock();
    stm8_pwm_init();
    stm8_gpio_led_init();
    stm8_gpio_switch_init();
    stm8_gpio_set_led(ON);
    
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*          STM8_FLY_CAM         *\n");
    stm8_uart_printf("*********************************\n");
    stm8_uart_printf("fly cam initialing...\n");
    stm8_uart_set_callback(uart_receiver);
    stm8_sensor_init();
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
#ifdef SONIC_USE
//    stm8_sonic_init();
#endif
    stm8_timer_set_interval(interval_function, 20 /*ms*/);
    __enable_interrupt();
    stm8_pwm_set_value(STM8_PWM1,20);
    stm8_pwm_set_value(STM8_PWM2,20);
    stm8_pwm_set_value(STM8_PWM3,20);
    stm8_pwm_set_value(STM8_PWM4,20);
    // uint32_t p = 0;
    while(1)
    {
        //stm8_gpio_toggle_led();
        // stm8_pwm_set_value(STM8_PWM1|STM8_PWM2|STM8_PWM3|STM8_PWM4,counter*50);
        // counter = (counter + 1)%10;
        // p++;
        // stm8_pwm_set_value(STM8_PWM1,p/500);
        // stm8_pwm_set_value(STM8_PWM2,p/500);
        // stm8_pwm_set_value(STM8_PWM3,p/500);
        // stm8_pwm_set_value(STM8_PWM4,p/500);
    }
}

stm8_angle_t gAngleX;
uint16_t counter = 0;

void interval_function(void)
{
    pid_update_pwm();
    counter++;
    if (flyingTime < FLYING_TIME_MAX)
    {
        stm8_gpio_set_led(ON);
    }
    else
    {
        if ((counter%25)==0)
        {
            stm8_gpio_toggle_led();
        }
    }
    if (counter == 50)
    {
        counter = 0;
        stm8_uart_printf("\nLog:");
        stm8_uart_print_u8((int)(lastAngleX - offsetAngleX));
        stm8_uart_print_u8((int)(lastAngleY - offsetAngleY));
        stm8_uart_print_u8((int)(lastAngleZ - offsetAngleZ));
        voltage = stm8_adc_read_channel(4)/78.05;
        stm8_uart_print_u8((int)(voltage*10));
        stm8_uart_print_u8((int)stm8_sonic_get_distance());
        // stm8_uart_print_float(kDistP);
        // stm8_uart_print_float(kDistI);
        // stm8_uart_print_float(kDistD);
        stm8_uart_printf("\n");
    }
#ifdef SONIC_USE
    if ((counter%5) == 0)
    {
        pid_update_distance();
        stm8_sonic_triger();
    }
#endif
}
