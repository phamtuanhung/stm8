
#include <stdint.h>
#include <iostm8s103f3.h>

#define ADJ_P 0
#define ADJ_I 1
#define ADJ_D 2

#define FLYING_TIME_MAX 40
#define ANGLE_MAX 45

extern int pwm;

extern uint8_t k_adjust;
extern float adjust;

extern float lastAngleX;
extern float lastAngleY;
extern float lastAngleZ;
extern float offsetAngleX;
extern float offsetAngleY;
extern float offsetAngleZ;

extern int pwm;
extern int pwmAdd;
extern int pwmVoltage;
extern int pid;

void pid_update_pwm(void);
void pid_calibrate(void);
void pid_adjust_k(int8_t c);
void pid_print_k(void);
void uart_receiver(char * message);
void pid_update_distance(void);
