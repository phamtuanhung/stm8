
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "stm8_pid.h"
#include "stm8_sensor.h"
#include "stm8_uart.h"
#include "stm8_pwm.h"
#include "stm8_timer.h"
#include "stm8_adc.h"
#include "stm8_sonic.h"

float lastAngleX = 0;
float lastAngleY = 0;
float lastAngleZ = 0;
float offsetAngleX = 0;
float offsetAngleY = 0;
float offsetAngleZ = 0;
// float errSumX = 0;
// float errSumY = 0;
// float errSumZ = 0;
float setpointX = 0;
float setpointY = 0;
float setpointZ = 0;


uint32_t flyingTime = FLYING_TIME_MAX;

#define K_P 0
#define K_I 1
#define K_D 2

float kpid[] = {0.6,0.0,0.6};

#define OFFSET_PWM_X 15
#define OFFSET_PWM_Y -20
#define OFFSET_TURN   -50
#define OFFSET_PWM   150
#define PWM_FLY   200
#define PWM_LAND  140

int offsetPwmX = OFFSET_PWM_X;//-25;
int offsetPwmY = OFFSET_PWM_Y;//20;
int offsetTurn = OFFSET_TURN;//20;
int offsetPwm = OFFSET_PWM;//20;

int pwm = 0;
int pwmAdd = 0;
int pwmDist = 0;
int pid = 0;
float distance;
uint8_t  zeroAccelCount = 0;
float zAaccel;
float zSpeed;
extern float gDistanceoffset;

#define RAD_TO_DEG 57.29577951308232087679815481

void pid_update_pwm(void)
{
    sensor_data_t sensor_data;
    sensor_data = stm8_sensor_get_value();
      
    float rollx = atan2( (int)sensor_data.accel_z, (int)sensor_data.accel_x) * RAD_TO_DEG - 90;
    float rolly = atan2( (int)sensor_data.accel_z, (int)-sensor_data.accel_y) * RAD_TO_DEG - 90;
//    float rollz = 0;
    
    float angle_x = 0.98 * (lastAngleX + ((float) ((int)sensor_data.gyro_y)/6550)) + 0.02 * ((rollx));
    float angle_y = 0.98 * (lastAngleY + ((float) ((int)sensor_data.gyro_x)/6550)) + 0.02 * ((rolly));
//    float angle_z = 0.998 * (lastAngleZ + ((float) ((int)sensor_data.gyro_z)/6550)) + 0.002 * ((rollz));
    
//    zAaccel = (((float)sensor_data.accel_z) / 16.707)/(cos(angle_x*0.0174)*cos(angle_y*0.0174)) - 980.66;
    zAaccel = ((float)sensor_data.accel_z)/16.707 - 980.66;
    if ((int)zAaccel == 0)
    {
        if (zeroAccelCount == 3)
            zSpeed = 0;
        zeroAccelCount++;      
    }
    else
    {
       zeroAccelCount = 0;
    }
    zSpeed += zAaccel*0.02; 
    
    float error_x = setpointX - (angle_x - offsetAngleX);
    // errSumX+= error_x;
    float de_x = (setpointX - angle_x) - (setpointX - lastAngleX); //de = (last_input - input);  
    int pwm_x = (int)(kpid[K_P] * error_x /* + kpid[K_I]*errSumX*0.02 */ + kpid[K_D]*de_x/0.02);/* P + I + D */
    
    float error_y = setpointY - (angle_y - offsetAngleY);
    // errSumY+= error_y;  
    float de_y = (setpointY - angle_y) - (setpointY - lastAngleY); //de = (last_input - input);  
    int pwm_y = (int)(kpid[K_P] * error_y /* + kpid[K_I]*errSumY*0.02 */ + kpid[K_D]*de_y/0.02);/* P + I + D */
    
//    float error_z = setpointZ - (angle_z - offsetAngleZ);
//     errSumZ+= error_z;  
//    float de_z = (setpointZ - angle_z) - (setpointZ - lastAngleZ); //de = (last_input - input);  
//    int pwm_z = (int)(1.5 * error_z + 1.5*de_z/0.02);/* P + I + D */
    
    lastAngleX = angle_x;
    lastAngleY = angle_y;
//    lastAngleZ = angle_z;
    pwm_x = -pwm_x;
    pwm_y = pwm_y;
//    pwm_z = -pwm_z;
    
    if (pwm != 0)
    {
        if ( flyingTime < FLYING_TIME_MAX)
        {
            flyingTime++;
        }
        else
        {
            pwm = 0;
            pid = 0;
        }
    }
    else {
        flyingTime =FLYING_TIME_MAX;
        pid = 0;
    }
    
    if (((angle_x - offsetAngleX) >  ANGLE_MAX)||((angle_y - offsetAngleY) >  ANGLE_MAX) ||
        ((angle_x - offsetAngleX) < -ANGLE_MAX)||((angle_y - offsetAngleY) < -ANGLE_MAX) 
        //|| (distance > 100)
        )
    {
        // offsetPwm = 0;
        flyingTime = FLYING_TIME_MAX;
        pwm = 0;
        pid = 0;
    }
    
    pwmAdd = 0;
//    pwm_z = 0;
    pwmDist = 0;
    stm8_pwm_set_value(STM8_PWM1,(pwm + pwmDist + (offsetTurn) /* + pwm_z*/)*pid + (pwm_x + offsetPwmX)*pid + (pwm_y+ offsetPwmY)*pid);
    stm8_pwm_set_value(STM8_PWM2,(pwm + pwmDist - (offsetTurn) /* - pwm_z*/)*pid - (pwm_x + offsetPwmX)*pid + (pwm_y+ offsetPwmY)*pid);
    stm8_pwm_set_value(STM8_PWM3,(pwm + pwmDist + (offsetTurn) /* + pwm_z*/)*pid - (pwm_x + offsetPwmX)*pid - (pwm_y+ offsetPwmY)*pid);
    stm8_pwm_set_value(STM8_PWM4,(pwm + pwmDist - (offsetTurn) /* - pwm_z*/)*pid + (pwm_x + offsetPwmX)*pid - (pwm_y+ offsetPwmY)*pid);

    // stm8_uart_printf("\n");
}

float offsetDist = 0;
float flyDist = 20;
float errDistSum;
float errDistLast;

float kDistP = 20;
float kDistI = 1;
float kDistD = 30;

#ifdef SONIC_USE
#define PWM_DIST_MAX 200
void pid_update_distance(void)
{
     float errDist;
     distance = offsetDist - stm8_sonic_get_distance();
     errDist = distance - flyDist;
     if ((pwm != 0)&&(distance > 3))
     {   
         if (abs((int)(errDist - errDistLast)) > 20)
         {
             errDist = errDistLast;
         }
         errDistSum += errDist;
         pwmDist = -(int)(kDistP*errDist + kDistI*errDistSum + kDistD*(errDist - errDistLast));
         if (pwmDist < -PWM_DIST_MAX) pwmDist = -PWM_DIST_MAX;
         if (pwmDist >  PWM_DIST_MAX) pwmDist =  PWM_DIST_MAX;
     }
     else
     {
         pwmDist = 0;
         errDistSum = 0;
         errDistLast = 0;
     }
     errDistLast = errDist;
}
#endif
void pid_calibrate(void)
{
    offsetAngleX = lastAngleX;
    offsetAngleY = lastAngleY;
    offsetAngleZ = lastAngleZ;
    offsetDist = stm8_sonic_get_distance();
}

static int pid_get_int(char ** buffer)
{
    uint8_t index;
    int ret;
    index = strstr(*buffer,"::") - *buffer;
    *buffer[index - 1] = 0;
    ret = atoi(*buffer);
    *buffer += index + 2;
    return ret;
}

static void pid_push_setting(char * str, int para1, int para2, int para3)
{
   stm8_uart_printf(str);
   stm8_uart_print_u8(para1);
   stm8_uart_printf(" ::");
   stm8_uart_print_u8(para2);
   stm8_uart_printf(" ::");
   stm8_uart_print_u8(para3);
   stm8_uart_printf(" ::\n");
}

void uart_receiver(char * message)
{
    char * buffer;
    uint8_t index;
//    stm8_uart_printf("rx: ");
//    stm8_uart_printf(message);
//    stm8_uart_printf("\n");
    if (strstr(message, "speed "))
    {
        buffer = strstr(message, "speed ") + sizeof("speed");
        index = strstr(buffer," ") - buffer;
        buffer[index] = 0;
        pwm = atoi(buffer);
        flyingTime = 0;
        pid = 1;
        
        // token = strtok(NULL, s);
        // setpointX = ((float) atoi(token))/10;
        
        // token = strtok(NULL, s);
        // setpointY = ((float) atoi(token))/10;
        
        //token = strtok(NULL, s);
        //setpointZ = atoi(token);
    }
    if (strstr(message, "calibrate"))
    {
        pid_calibrate();
    }
    buffer = strstr(message, "offset: ");
    if (buffer)
    {
        buffer += sizeof("offset:");
        offsetPwmX = pid_get_int(&buffer);
        offsetPwmY = pid_get_int(&buffer);
        offsetTurn = pid_get_int(&buffer);
    }
    buffer = strstr(message, "set_pid: ");
    if (buffer)
    {
        buffer += sizeof("set_pid:");
        kpid[0] = (float)pid_get_int(&buffer)/10;
        kpid[1] = (float)pid_get_int(&buffer)/10;
        kpid[2] = (float)pid_get_int(&buffer)/10;
    }
    buffer = strstr(message, "set_kHeigh: ");
    if (buffer)
    {
        buffer += sizeof("set_kHeigh:");
        kDistP = pid_get_int(&buffer);
        kDistI = pid_get_int(&buffer);
        kDistD = pid_get_int(&buffer);
    }
    if (strstr(message, "get pid "))
    {
      pid_push_setting("pid: ",(int)(kpid[0]*10),(int)(kpid[1]*10),(int)(kpid[2]*10));
      pid_push_setting("offset: ", offsetPwmX, offsetPwmY,offsetTurn);
      pid_push_setting("kHeigh: ", (int)(kDistP*10), (int)(kDistI*10),(int)(kDistD*10));
      
        stm8_uart_printf("pid: ");
        stm8_uart_print_u8((int)(kpid[0]*10));
        stm8_uart_printf(" ::");
        stm8_uart_print_u8((int)(kpid[1]*10));
        stm8_uart_printf(" ::");
        stm8_uart_print_u8((int)(kpid[2]*10));
        stm8_uart_printf(" ::\n");
        
        stm8_uart_printf("offset: ");
        stm8_uart_print_u8(offsetPwmX);
        stm8_uart_printf(" ::");
        stm8_uart_print_u8(offsetPwmY);
        stm8_uart_printf(" ::");
        stm8_uart_print_u8(offsetTurn);
        stm8_uart_printf(" ::\n");
        
        stm8_uart_printf("kHeigh: ");
        stm8_uart_print_u8((int)(kDistP));
        stm8_uart_printf(" ::");
        stm8_uart_print_u8((int)(kDistI));
        stm8_uart_printf(" ::");
        stm8_uart_print_u8((int)(kDistD));
        stm8_uart_printf(" ::\n");
    }
    //if (strstr(message, "left_right "))
    //{
    //    stm8_uart_printf("lr: ");
    //    const char s[2] = " ";
    //    char *token;
    //    token = strtok(message, s);
    //    token = strtok(NULL, s);
    //    
    //    int left = atoi(token);
    //    stm8_uart_print_int(left);
    //    stm8_motor_left(left);
    //    
    //    token = strtok(NULL, s);
    //    int right = atoi(token);
    //    stm8_uart_print_int(right);
    //    stm8_motor_right(right);
    //    stm8_uart_printf("\n");
    //}
}

