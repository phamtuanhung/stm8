/* metter.c */
#include <intrinsics.h>
#include <intrinsics.h>
#include <math.h>
#include <string.h>

#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_sonic.h"

void interval_function(void);

uint16_t timer;
uint32_t counter = 0;

void main( void )
{
    InitialiseSystemClock();
    stm8_gpio_led_init();
    stm8_gpio_switch_init();
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*          STM8_CONTROLLER         *\n");
    stm8_uart_printf("*********************************\n");
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
    stm8_timer_set_interval(interval_function, 100 /*ms*/);
    stm8_sonic_init();
    __enable_interrupt();
    while(1)
    {
        // if (PA_IDR_IDR1 == 0)
        // {
            // counter++;
        // }
        // else
        // {
            // if (counter != 0)
            // {
                // stm8_uart_print_int(counter);
                // stm8_uart_printf("\n");
            // }
            // counter = 0;
        // }
    }
}

void interval_function(void)
{
    stm8_sonic_triger();
    counter++;
    if ((counter%5)==0)
    {
        stm8_gpio_toggle_led();
        stm8_uart_print_float(stm8_sonic_get_distance());
        stm8_uart_printf("\r");
    }
}

