/* main.c */
#include <intrinsics.h>
#include <intrinsics.h>
#include <math.h>
#include <string.h>

// #define STM8_PWM_MAX 300

#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_pwm.h"
#include "stm8_sensor.h"
#include "stm8_ir.h"
#include "stm8_angle.h"
#include "stm8_adc.h"
#include "stm8_pid.h"

void interval_function(void);
void ir_receiver(char * data);
void print_sensor_data(sensor_data_t sensor);

float voltage;

void main( void )
{
    InitialiseSystemClock();
    stm8_gpio_init(); 
    stm8_gpio_set_led(ON);
    stm8_gpio_exti_set_callback(stm8_ir_signal_analysis);
    stm8_ir_set_callback(pid_ir_data);
    
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*        STM8_QUADCOPTER        *\n");
    stm8_uart_printf("*********************************\n");
    stm8_sensor_init();
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
    stm8_timer_set_interval(interval_function, 20 /*ms*/);
    stm8_pwm_init();
    __enable_interrupt();
    uint16_t timeout = 0;
    while(1)
    {
        //stm8_timer_delay(500);
        if (irReceiving == 1)
        {
            
            if (timeout < 65000)
            {
                stm8_timer_interval_disable();
            }
            else
            {
                stm8_timer_interval_enable();
                irReceiving = 0;
                timeout = 0;
            }
            stm8_gpio_set_led(ON);
            timeout++;
        }
        else
        {
            stm8_timer_interval_enable();
            timeout = 0;
        }
        //stm8_gpio_toggle_led();
        // stm8_pwm_set_value(STM8_PWM1|STM8_PWM2|STM8_PWM3|STM8_PWM4,counter*50);
        // counter = (counter + 1)%10;
    }
}

stm8_angle_t gAngleX;
uint16_t counter = 0;

void interval_function(void)
{
    pid_update_pwm();
    counter++;
    if (counter == 25)
    {
        stm8_gpio_toggle_led();
        counter = 0;
    }
    stm8_uart_printf("ADC = ");
    voltage = stm8_adc_read_channel(4)/77.17;
    stm8_uart_print_float(voltage);
    stm8_uart_printf("\n");
}
void ir_receiver(char * data)
{
    
}

void print_sensor_data(sensor_data_t sensor)
{
    stm8_uart_printf("    Accel: ");
    stm8_uart_print_int(sensor.accel_x);
    stm8_uart_printf(", ");
    stm8_uart_print_int(sensor.accel_y);
    stm8_uart_printf(", ");
    stm8_uart_print_int(sensor.accel_z);
    
    stm8_uart_printf("   Gyro: ");
    stm8_uart_print_int(sensor.gyro_x);
    stm8_uart_printf(", ");         
    stm8_uart_print_int(sensor.gyro_y);
    stm8_uart_printf(", ");         
    stm8_uart_print_int(sensor.gyro_z);
}
