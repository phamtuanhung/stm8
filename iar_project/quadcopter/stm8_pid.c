
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "stm8_pid.h"
#include "stm8_sensor.h"
#include "stm8_uart.h"
#include "stm8_pwm.h"
#include "stm8_timer.h"
#include "stm8_adc.h"

double lastAngleX = 0;
double lastAngleY = 0;
double lastAngleZ = 0;
double offsetAngleX = 0;
double offsetAngleY = 0;
double offsetAngleZ = 0;
double errSumX = 0;
double errSumY = 0;
double errSumZ = 0;
double setpointX = 0;
double setpointY = 0;
double setpointZ = 0;


uint32_t flyingTime = 0;
#define FLYING_TIME_MAX 400
#define ANGLE_MAX 15

#define K_P 0
#define K_I 1
#define K_D 2

double kpid[] = {0.4,0,0.4};
uint8_t ka[3][3] = {{0,1,0},{0,0,0},{0,1,0}};
uint8_t a = 1;
uint8_t k = 0;
char * sa[] = {" ( _X.X )"," ( X_.X ) "," ( XX._ )"};
char * sk[] = {"update kp","update ki","update kd"};

#define OFFSET_PWM_X 8
#define OFFSET_PWM_Y 17
#define OFFSET_PWM   0
#define PWM_FLY   385
#define PWM_LAND  140
#define TURN_PWM   40

int offsetPwmX = OFFSET_PWM_X;//-25;
int offsetPwmY = OFFSET_PWM_Y;//20;
int offsetPwm = OFFSET_PWM;//20;
int turnPwm = TURN_PWM;//20;

int pwm = 0;
int pwmAdd = 0;
int pwmVoltage = 0;
int pid = 0;
uint8_t k_adjust = ADJ_P;
double adjust = 1;
uint8_t adjusting = 0;

#define RAD_TO_DEG 57.29577951308232087679815481

void pid_update_pwm(void)
{
    sensor_data_t sensor_data;
    sensor_data = stm8_sensor_get_value();
    
    double rollx = atan2( (int)sensor_data.accel_z, (int)sensor_data.accel_x) * RAD_TO_DEG - 90;
    double rolly = atan2( (int)sensor_data.accel_z, (int)-sensor_data.accel_y) * RAD_TO_DEG - 90;
    double rollz = 0;
    
    double angle_x = 0.98 * (lastAngleX + ((double) ((int)sensor_data.gyro_y)/6550)) + 0.02 * ((rollx));
    double angle_y = 0.98 * (lastAngleY + ((double) ((int)sensor_data.gyro_x)/6550)) + 0.02 * ((rolly));
    double angle_z = 0.998 * (lastAngleZ + ((double) ((int)sensor_data.gyro_z)/6550)) + 0.002 * ((rollz));
    
    // double angle_x = rollx;
    // double angle_y = rolly;
    
    double error_x = setpointX - (angle_x - offsetAngleX);
    errSumX+= error_x;
    double de_x = (setpointX - angle_x) - (setpointX - lastAngleX); //de = (last_input - input);  
    int pwm_x = (int)(kpid[K_P] * error_x + kpid[K_I]*errSumX*0.02 + kpid[K_D]*de_x/0.02);/* P + I + D */
    
    double error_y = setpointY - (angle_y - offsetAngleY);
    errSumY+= error_y;  
    double de_y = (setpointX - angle_y) - (setpointY - lastAngleY); //de = (last_input - input);  
    int pwm_y = (int)(kpid[K_P] * error_y + kpid[K_I]*errSumY*0.02 + kpid[K_D]*de_y/0.02);/* P + I + D */
    
    double error_z = setpointZ - (angle_z - offsetAngleZ);
    errSumZ+= error_z;  
    double de_z = (setpointZ - angle_z) - (setpointZ - lastAngleZ); //de = (last_input - input);  
    int pwm_z = (int)(0.5 * error_z + 0*errSumZ*0.02 + 0.5*de_z/0.02);/* P + I + D */
    
    lastAngleX = angle_x;
    lastAngleY = angle_y;
    lastAngleZ = angle_z;
    pwm_x = -pwm_x;
    pwm_y = pwm_y;
    pwm_z = -pwm_z;
    // pwm_z = 0;
    
    if (pwm == PWM_FLY)
    {
        if (flyingTime > 10)
        {
            pwmVoltage = (462 - (int)(voltage*60)) + flyingTime/16;
        }
        else
        {
            pwmVoltage = 0;
        }
        
        if (flyingTime < FLYING_TIME_MAX)
        {
            flyingTime++;
        }
        else
        {
            flyingTime = 0;
            pwm = pwm - PWM_LAND;
            pwmVoltage = 0;
            // pid = 0;
        }
    }
    
    if (((angle_x - offsetAngleX) >  ANGLE_MAX)||((angle_y - offsetAngleY) >  ANGLE_MAX) ||
        ((angle_x - offsetAngleX) < -ANGLE_MAX)||((angle_y - offsetAngleY) < -ANGLE_MAX) )
    {
        // offsetPwm = 0;
        flyingTime = 0;
        pwm = 0;
        pid = 0;
    }
    
    // stm8_uart_printf("ADC = ");
    // stm8_uart_print_float(voltage);
    // stm8_uart_printf("\n");
    
    // stm8_uart_printf("pwmVoltage = ");
    // stm8_uart_print_int(pwmVoltage);
    // stm8_uart_printf(":");
    stm8_pwm_set_value(STM8_PWM1,(offsetPwm + turnPwm + pwm_z + pwm + pwmAdd + pwmVoltage)*pid + (pwm_x + offsetPwmX)*pid + (pwm_y + offsetPwmY)*pid);
    stm8_pwm_set_value(STM8_PWM2,(offsetPwm - turnPwm - pwm_z + pwm + pwmAdd + pwmVoltage)*pid - (pwm_x + offsetPwmX)*pid + (pwm_y + offsetPwmY)*pid);
    stm8_pwm_set_value(STM8_PWM3,(offsetPwm + turnPwm + pwm_z + pwm + pwmAdd + pwmVoltage)*pid - (pwm_x + offsetPwmX)*pid - (pwm_y + offsetPwmY)*pid);
    stm8_pwm_set_value(STM8_PWM4,(offsetPwm - turnPwm - pwm_z + pwm + pwmAdd + pwmVoltage)*pid + (pwm_x + offsetPwmX)*pid - (pwm_y + offsetPwmY)*pid);
    // stm8_uart_printf("pwm_x: ");
    // stm8_uart_print_int(pwm_x);
    // stm8_uart_printf("    pwm_y: ");
    // // stm8_uart_print_int(pwm_y);
    // stm8_uart_printf("    pwm_z: ");
    // stm8_uart_print_int(pwm_z);
  
    // stm8_uart_printf("   angle_x: ");
    // stm8_uart_print_float(angle_x - offsetAngleX);
    // stm8_uart_printf("    angle_y: ");
    // stm8_uart_print_float(angle_y - offsetAngleY);
    // stm8_uart_printf("    angle_z: ");
    // stm8_uart_print_float(angle_z - offsetAngleZ);
  
    // stm8_uart_printf("    Accel: ");
    // stm8_uart_print_int(sensor_data.accel_x);
    // stm8_uart_printf(", ");
    // stm8_uart_print_int(sensor_data.accel_y);
    // stm8_uart_printf(", ");
    // stm8_uart_print_int(sensor_data.accel_z);
    
    // stm8_uart_printf("   Gyro: ");
    // stm8_uart_print_int(sensor_data.gyro_x);
    // stm8_uart_printf(", ");         
    // stm8_uart_print_int(sensor_data.gyro_y);
    // stm8_uart_printf(", ");         
    // stm8_uart_print_int(sensor_data.gyro_z);
  
    // stm8_uart_printf("\n");
}

void pid_ir_data(char * data)
{
    if (strstr(data, "center"))
    {
        if (adjusting == 1)
        {
            k = 1;
            pid_print_k();
        }
        else
        {
            pid = 1;
            pwm = PWM_FLY;
            offsetPwmX = OFFSET_PWM_X;
            offsetPwmY = OFFSET_PWM_Y;
            turnPwm = TURN_PWM;
        }
    }
    else if ((data[0] >= '0')&&(data[0] <= '9'))
    {
        if (adjusting == 1)
        {
            pid_adjust_k((uint8_t)(data[0] - '0'));
        }
        else
        {
            // if (data[0]=='9')
            // {
                // pid = 0;
            // }
            // else
            {
                offsetPwm = ((int)(data[0] - '0'))*5;
                //pid = 1;
                stm8_uart_print_int(offsetPwm);
                if (data[0]=='0')
                {
                    // pid = 0;
                }
                stm8_uart_printf("\n");
                
            }
        }
    }
    else if (strstr(data,"left"))
    {
        if (adjusting == 1)
        {
            if (a > 0)
                a = a-1;
            pid_print_k();
        }
        else
        {
            offsetPwmY += 2;
        }
    }
    else if (strstr(data,"right"))
    {
        if (adjusting == 1)
        {
            if (a < 2)
                a = a+1;
            pid_print_k();
        }
        else
        {
            offsetPwmY -= 2;
        }
    }
    else if (strstr(data,"up"))
    {
        if (adjusting == 1)
        {
            k = 0;
            pid_print_k();
        }
        else
        {
            offsetPwmX += 2;
        }
    }
    else if (strstr(data,"down"))
    {
        if (adjusting == 1)
        {
            k = 2;
            pid_print_k();
        }
        else
        {
            offsetPwmX -= 2;
        }
    }
    else if (strstr(data,"option"))
    {
        adjusting = 1;
        pid_print_k();
    }
    else if (strstr(data,"return"))
    {
        adjusting = 0;
        stm8_uart_printf("Stop adjust\n");
    }
    else if (strstr(data,"v+"))
    {
        pwmAdd += 5;
    }
    else if (strstr(data,"v-"))
    {
        pwmAdd -= 5;
    }
    else if (strstr(data,"p+"))
    {
        turnPwm += 2;
    }
    else if (strstr(data,"p-"))
    {
        turnPwm -= 2;
    }
    else if (strstr(data,"ooo"))
    {
        pid_calibrate();
        pid = 0;
        offsetPwmX = OFFSET_PWM_X;
        offsetPwmY = OFFSET_PWM_Y;
        turnPwm = TURN_PWM;
        pwmAdd = 0;
    }
    else if (strstr(data,"xxx"))
    {
        pwm = pwm - PWM_LAND;
    }
    else
    {
        stm8_uart_printf("ir: ");
        stm8_uart_printf(data);
        stm8_uart_printf("\n");
    }
    stm8_timer_delay(50);
}

void pid_calibrate(void)
{
    offsetAngleX = lastAngleX;
    offsetAngleY = lastAngleY;
}


void pid_adjust_k(int8_t c)
{
    ka[k][a] = c;
    kpid[k] = ka[k][0]*10 + ka[k][1] + ka[k][2]*0.1;
    pid_print_k();
}
void pid_update_k(void)
{
  
}
void pid_print_k(void)
{
    stm8_uart_printf(sk[k]);
    stm8_uart_printf(" = ");
    stm8_uart_print_float(kpid[k]);
    stm8_uart_printf(sa[a]);
    stm8_uart_printf("\n");
    
    // stm8_uart_printf("    angle_x: ");
    // stm8_uart_print_float(setpointX - lastAngleX);
    // stm8_uart_printf("    angle_y: ");
    // stm8_uart_print_float(setpointY - lastAngleY);
    
    // stm8_uart_printf("\n");
}
