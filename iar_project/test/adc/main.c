/* test_adc: main.c */
#include <intrinsics.h>
#include <intrinsics.h>
#include "stm8_gpio.h"
#include "stm8_uart.h"
#include "stm8_timer.h"
#include "stm8_adc.h"

void interval_function(void);

void main( void )
{
    InitialiseSystemClock();
    stm8_gpio_init(); 
    stm8_gpio_set_led(ON);
    stm8_uart_init();
    stm8_uart_printf("\n\n*********************************\n");
    stm8_uart_printf("*        STM8_TEST_ADC          *\n");
    stm8_uart_printf("*********************************\n");
    stm8_gpio_set_led(OFF);
    stm8_timer_init();
    stm8_timer_set_interval(interval_function, 500 /*ms*/);
    __enable_interrupt();
 
    while(1)
    {
        
    }
}

void interval_function(void)
{
    stm8_gpio_toggle_led();
    stm8_uart_printf("ADC = ");
    stm8_uart_print_int(readADC1(4));
    stm8_uart_printf("\n");
}
