/* stm8_ir.h */

#include <stdint.h>
#include <stdio.h>
#include <iostm8s103f3.h>
#include "stm8_timer.h"

typedef void (*stm8_ir_callback_function_t)(char * data);
extern uint8_t irReceiving;

void stm8_ir_signal_analysis(void);
void stm8_ir_receiver(int data);
void stm8_ir_set_callback(stm8_ir_callback_function_t function);
void stm8_ir_dump_callback(char * data);
