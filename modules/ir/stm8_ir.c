/* stm8_ir.c */

#include "stm8_ir.h"
#include "stm8_uart.h"

int irData = 0;
uint8_t irCount = 0;
uint8_t irReceiving = 0;

void stm8_ir_signal_analysis(void)
{
    uint16_t data = stm8_timer_couter_get_loop();
    if ((irReceiving == 0)&&(data>270)&&(data<330))
    {
        irReceiving = 1;
        //stm8_timer_interval_disable();
    }
    if (irReceiving == 1)
    {
        if (data > 350)
        {
            //printf("irData: %d\n",irData);
            stm8_ir_receiver(irData);
            irCount = 0;
            irReceiving = 0;
            irData=0;
            //stm8_timer_interval_enable();
        }
        else
        {
        if ((irCount>0)&&(irCount%2)==0)
            if (data > 100)
            {
                irData |= (1<<(irCount/2));
            }
        irCount++;
        }
    }
    //uart_printf("data: ");
    //uart_print_int(data);
    //uart_printf("\n");
}

stm8_ir_callback_function_t irCallbackPointer = stm8_ir_dump_callback;

void stm8_ir_set_callback(stm8_ir_callback_function_t function)
{
    irCallbackPointer = function;
}

void stm8_ir_dump_callback(char * data)
{
    
}

void stm8_ir_receiver(int data)
{
    switch (data)
    {
        case 292: (*irCallbackPointer)("v+"); break;
        case 294: (*irCallbackPointer)("v-"); break;
        case 374: (*irCallbackPointer)("ooo"); break;
        case 296: (*irCallbackPointer)("xxx"); break;
        case 288: (*irCallbackPointer)("p+"); break;
        case 290: (*irCallbackPointer)("p-"); break;
            
        case -26826: (*irCallbackPointer)("fastB"); break;
        case -26828: (*irCallbackPointer)("play"); break;
        case -26824: (*irCallbackPointer)("fastF"); break;
        case -26760: (*irCallbackPointer)("prev"); break;
        case -26830: (*irCallbackPointer)("pause");  break;
        case -26832: (*irCallbackPointer)("stop"); break;
        case -26758: (*irCallbackPointer)("next"); break;
        
        case 256: (*irCallbackPointer)("1"); break;
        case 258: (*irCallbackPointer)("2"); break;
        case 260: (*irCallbackPointer)("3"); break;
        case 262: (*irCallbackPointer)("4"); break;
        case 264: (*irCallbackPointer)("5"); break;
        case 266: (*irCallbackPointer)("6"); break;
        case 268: (*irCallbackPointer)("7"); break;
        case 270: (*irCallbackPointer)("8"); break;
        case 272: (*irCallbackPointer)("9"); break;
        case 274: (*irCallbackPointer)("0"); break;
        
        case 488: (*irCallbackPointer)("up"); break;
        case 490: (*irCallbackPointer)("down"); break;
        case 360: (*irCallbackPointer)("left"); break;
        case 358: (*irCallbackPointer)("right"); break;
        case 458: (*irCallbackPointer)("center"); break;
        
        case -26772: (*irCallbackPointer)("option"); break;
        case -26810: (*irCallbackPointer)("return"); break;
    }
    // stm8_uart_print_int(data);
    // stm8_uart_printf("\n");
}