/* stm8_motor.h */

#include <stdint.h>
#include <iostm8s103f3.h>
#include <stdlib.h>
#include "stm8_pwm.h"

#define R_FORW STM8_PWM2
#define R_BACK STM8_PWM1
#define L_FORW STM8_PWM3
#define L_BACK STM8_PWM4

void stm8_motor_left(int speed);
void stm8_motor_right(int speed);
void stm8_motor_speed(int speed);
void stm8_motor_turn(int speed, int angle);
void stm8_motor_turn_left(int speed, int angle);
void stm8_motor_turn_right(int speed, int angle);


