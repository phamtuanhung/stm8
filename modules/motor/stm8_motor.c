/* stm8_motor.c */

#include "stm8_motor.h"

void stm8_motor_left(int speed)
{
    if (speed > 0)
        stm8_pwm_set_value(L_FORW, speed);
    if (speed < 0)
        stm8_pwm_set_value(L_BACK, -speed);
    if (speed == 0)
    {
        stm8_pwm_set_value(L_FORW,0);
        stm8_pwm_set_value(L_BACK,0);
    }
}

void stm8_motor_right(int speed)
{
    if (speed > 0)
        stm8_pwm_set_value(R_FORW, speed);
    if (speed < 0)
        stm8_pwm_set_value(R_BACK, -speed);
    if (speed == 0)
    {
        stm8_pwm_set_value(R_FORW,0);
        stm8_pwm_set_value(R_BACK,0);
    }
}


void stm8_motor_speed(int speed)
{
    stm8_motor_left(speed);
    stm8_motor_right(speed);
}

void stm8_motor_turn(int speed, int angle)
{
    if (speed >=0)
    {
        stm8_motor_left(speed + angle);
        stm8_motor_right(speed - angle);
    }
    else
    {
        stm8_motor_left(speed - angle);
        stm8_motor_right(speed + angle);
    }
}

void stm8_motor_turn_left(int speed, int angle)
{
    stm8_motor_turn(speed,-angle);
}

void stm8_motor_turn_right(int speed, int angle)
{
    stm8_motor_turn(speed,+angle);
}
