/* stm8_angle->c */

#include "stm8_angle.h"

#define RAD_TO_DEG 57.29577951308232087679815481
#define TIME_CONSTANT 0.5


double stm8_angle_get_value(stm8_angle_t * angle,int gyro, int accelGravity, int accelRoll, double dt)
{
    double roll  = atan2(accelGravity, accelRoll) * RAD_TO_DEG + 90;
    //double A = TIME_CONSTANT/(TIME_CONSTANT+dt);
    angle->value = 0.98 * (angle->last + ((double) (gyro)/(13100))) + 0.02 * ((roll));
    // double angle_x = 0.98 * (last_angle_x + ((double) ((int)sensor_data.gyro_y)/13100)) + 0.02 * ((rollx));
    angle->last = angle->value;
    return (angle->value - angle->offset);
}

void stm8_angle_get_xy(stm8_angle_xyz_t* angle)
{
   sensor_data_t sensor_data;
   angle->x.last = angle->x.value;
   angle->y.last = angle->y.value;
   sensor_data = stm8_sensor_get_value();
   double rollx = atan2( (int)sensor_data.accel_z, (int)sensor_data.accel_x) * RAD_TO_DEG - 90;
   double rolly = atan2( (int)sensor_data.accel_z, (int)-sensor_data.accel_y) * RAD_TO_DEG - 90;
    
   angle->x.value = 0.98 * (angle->x.last + ((double) ((int)sensor_data.gyro_y)/6550)) + 0.02 * ((rollx));
   angle->y.value = 0.98 * (angle->y.last + ((double) ((int)sensor_data.gyro_x)/6550)) + 0.02 * ((rolly));
}
