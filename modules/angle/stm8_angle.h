/* stm8_angle.h */

#include <stdlib.h>
#include <math.h>
#include <iostm8s103f3.h>
#include "stm8_sensor.h"

typedef struct {
    double value;
    double last;
    double offset;
} stm8_angle_t;

typedef struct {
    stm8_angle_t x;
    stm8_angle_t y;
    stm8_angle_t z;
} stm8_angle_xyz_t;

double stm8_angle_get_value(stm8_angle_t * angle,int gyro, int accelGravity, int accelRoll, double dt);
void stm8_angle_get_xy(stm8_angle_xyz_t* angle);


