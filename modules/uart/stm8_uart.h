/* stm8_uart.h */

#include <stdio.h>
#include <iostm8s103f3.h>
#include <stdlib.h>

typedef void (*stm8_uart_callback_function_t)(char * data);

void stm8_uart_init(void);
void stm8_uart_print(char *message, unsigned char lenght);
void stm8_uart_printf(char *message);
void stm8_uart_put_char(unsigned char c);
void stm8_uart_print_float(double f);
void stm8_uart_print_int(int i);
void stm8_uart_print_u8(int i);
void stm8_uart_rx_proc(char * message);
void stm8_uart_set_callback(stm8_uart_callback_function_t function);
void InitialiseSystemClock(void);
