/* stm8_uart.c */

#include "stm8_uart.h"

void stm8_uart_init(void)
{
  //
    //  Clear the Idle Line Detected bit in the status register by a read
    //  to the UART1_SR register followed by a Read to the UART1_DR register.
    //
    unsigned char tmp = UART1_SR;
    tmp = UART1_DR;
    //
    //  Reset the UART registers to the reset values.
    //
    UART1_CR1 = 0;
    UART1_CR2 = 0;
    UART1_CR4 = 0;
    UART1_CR3 = 0;
    UART1_CR5 = 0;
    UART1_GTR = 0;
    UART1_PSCR = 0;
    //
    //  Now setup the port to 115200,n,8,1.
    //
    UART1_CR1_M = 0;        //  8 Data bits.
    UART1_CR1_PCEN = 0;     //  Disable parity.
    UART1_CR3_STOP = 0;     //  1 stop bit.
    UART1_BRR2 = 0x0a;      //  Set the baud rate registers to 115200 baud
    UART1_BRR1 = 0x08;      //  based upon a 16 MHz system clock.
    //
    //  Disable the transmitter and receiver.
    //
    UART1_CR2_TEN = 0;      //  Disable transmit.
    UART1_CR2_REN = 0;      //  Disable receive.
    //
    //  Set the clock polarity, lock phase and last bit clock pulse.
    //
    UART1_CR3_CPOL = 1;
    UART1_CR3_CPHA = 1;
    UART1_CR3_LBCL = 1;
    //
    //  Turn on the UART transmit, receive and the UART clock.
    //
    UART1_CR2_TEN = 1;
    UART1_CR2_REN = 1;
    UART1_CR3_CKEN = 1;
    
    InitialiseSystemClock();
}

void InitialiseSystemClock(void)
{
    CLK_ICKR = 0;                       //  Reset the Internal Clock Register.
    CLK_ICKR_HSIEN = 1;                 //  Enable the HSI.
    CLK_ECKR = 0;                       //  Disable the external clock.
    while (CLK_ICKR_HSIRDY == 0);       //  Wait for the HSI to be ready for use.
    CLK_CKDIVR = 0;                     //  Ensure the clocks are running at full speed.
    CLK_PCKENR1 = 0xff;                 //  Enable all peripheral clocks.
    CLK_PCKENR2 = 0xff;                 //  Ditto.
    CLK_CCOR = 0;                       //  Turn off CCO.
    CLK_HSITRIMR = 0;                   //  Turn off any HSIU trimming.
    CLK_SWIMCCR = 0;                    //  Set SWIM to run at clock / 2.
    CLK_SWR = 0xe1;                     //  Use HSI as the clock source.
    CLK_SWCR = 0;                       //  Reset the clock switch control register.
    CLK_SWCR_SWEN = 1;                  //  Enable switching.
    while (CLK_SWCR_SWBSY != 0);        //  Pause while the clock switch is busy.
}

void stm8_uart_printf(char *message)
{
    char *ch = message;
    while (*ch)
    {
        stm8_uart_put_char(*ch);     //  Put the next character into the data transmission function.
        while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
        ch++;                               //  Grab the next character.
    }
}
void stm8_uart_print_float(double f)
{
  int i = (int) (f*10);
  if (i < 0)
    stm8_uart_put_char('-');
  else
   stm8_uart_put_char(' ');
  i = abs(i);
  i = i%1000;
  stm8_uart_put_char(i/100+'0');
  i = i%100;
  stm8_uart_put_char(i/10+'0');
  stm8_uart_put_char('.');
  i = i %10;
  stm8_uart_put_char(i+'0');
}
void stm8_uart_print_int(int i)
{
  if (i < 0)
    stm8_uart_put_char('-');
  else
   stm8_uart_put_char(' ');
  i = abs(i);
  i = i%1000000;
  stm8_uart_put_char(i/100000+'0');
  i = i%100000;
  stm8_uart_put_char(i/10000+'0');
  i = i%10000;
  stm8_uart_put_char(i/1000+'0');
  i = i%1000;
  stm8_uart_put_char(i/100+'0');
  i = i%100;
  stm8_uart_put_char(i/10+'0');
  i = i %10;
  stm8_uart_put_char(i+'0');
}
void stm8_uart_put_char(unsigned char c)
{
  UART1_DR = (unsigned char) c;     //  Put character into the data transmission register.
  while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
  if (c == '\n')
    UART1_DR = (unsigned char) '\r';     //  Put character into the data transmission register.
  while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
}
