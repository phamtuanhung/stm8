/* stm8_uart.c */
#include <stdlib.h>
#include <string.h>

#include "stm8_uart.h"
unsigned char u8Mode = 0;

void stm8_uart_init(void)
{
  //
    //  Clear the Idle Line Detected bit in the status register by a read
    //  to the UART1_SR register followed by a Read to the UART1_DR register.
    //
    unsigned char tmp = UART1_SR;
    tmp = UART1_DR;
    //
    //  Reset the UART registers to the reset values.
    //
    UART1_CR1 = 0;
    UART1_CR2 = 0;
    UART1_CR4 = 0;
    UART1_CR3 = 0;
    UART1_CR5 = 0;
    UART1_GTR = 0;
    UART1_PSCR = 0;
    //
    //  Now setup the port to 115200,n,8,1.
    //
    UART1_CR1_M = 0;        //  8 Data bits.
    UART1_CR1_PCEN = 0;     //  Disable parity.
    UART1_CR3_STOP = 0;     //  1 stop bit.
    UART1_BRR2 = 0x1a;      //  Set the baud rate registers to 2400 baud
    UART1_BRR1 = 0xa0;      //  based upon a 16 MHz system clock.
    // UART1_BRR2 = 0x0a;      //  Set the baud rate registers to 115200 baud
    // UART1_BRR1 = 0x08;      //  based upon a 16 MHz system clock.
    //
    //  Disable the transmitter and receiver.
    //
    UART1_CR2_TEN = 0;      //  Disable transmit.
    UART1_CR2_REN = 0;      //  Disable receive.
    //
    //  Set the clock polarity, lock phase and last bit clock pulse.
    //
    UART1_CR3_CPOL = 1;
    UART1_CR3_CPHA = 1;
    UART1_CR3_LBCL = 1;
    //  Set the Receive Interrupt RM0016 p358,362
    UART1_CR2_TIEN  = 0;     // Transmitter interrupt enable
    UART1_CR2_TCIEN = 0;     // Transmission complete interrupt enable
    UART1_CR2_RIEN  = 1;     //  Receiver interrupt enable
    UART1_CR2_ILIEN = 0;     //  IDLE Line interrupt enable

    //  Turn on the UART transmit, receive and the UART clock.
    UART1_CR2_TEN    = 1;
    UART1_CR2_REN    = 1;
    UART1_CR1_PIEN   = 0;
    UART1_CR4_LBDIEN = 0;
    
    InitialiseSystemClock();
}
char sInputBuffer[50];
unsigned char uInputLenght = 0;

#pragma vector = UART1_R_RXNE_vector
__interrupt void UART1_IRQHandler(void)
{
    unsigned char recd;
    recd = UART1_DR;
    if (uInputLenght < 49)
    {
        sInputBuffer[uInputLenght] = recd;
        uInputLenght++;
        if (recd == '\r')
        {
            sInputBuffer[uInputLenght - 1] = 0;
            stm8_uart_rx_proc(sInputBuffer);
            uInputLenght = 0;
        }
    }
    else
    {
        uInputLenght = 0;
    }
    if (u8Mode == 1)
        stm8_uart_put_char(recd);
}
void stm8_uart_dump_callback(char * data)
{
    
}
stm8_uart_callback_function_t uartCallbackPointer = stm8_uart_dump_callback;

void stm8_uart_rx_proc(char * message)
{
    if (strstr(message, "debug on"))
    {
        stm8_uart_printf("Enter Debug mode");
        u8Mode = 1;
    }
    if (strstr(message, "debug off"))
    {
        stm8_uart_printf("\nExit Debug mode\n");
        u8Mode = 0;
    }
    (*uartCallbackPointer)(message);
}

void stm8_uart_set_callback(stm8_uart_callback_function_t function)
{
    uartCallbackPointer = function;
}
    
void InitialiseSystemClock(void)
{
    CLK_ICKR = 0;                       //  Reset the Internal Clock Register.
    CLK_ICKR_HSIEN = 1;                 //  Enable the HSI.
    CLK_ECKR = 0;                       //  Disable the external clock.
    while (CLK_ICKR_HSIRDY == 0);       //  Wait for the HSI to be ready for use.
    CLK_CKDIVR = 0;                     //  Ensure the clocks are running at full speed.
    CLK_PCKENR1 = 0xff;                 //  Enable all peripheral clocks.
    CLK_PCKENR2 = 0xff;                 //  Ditto.
    CLK_CCOR = 0;                       //  Turn off CCO.
    CLK_HSITRIMR = 0;                   //  Turn off any HSIU trimming.
    CLK_SWIMCCR = 0;                    //  Set SWIM to run at clock / 2.
    CLK_SWR = 0xe1;                     //  Use HSI as the clock source.
    CLK_SWCR = 0;                       //  Reset the clock switch control register.
    CLK_SWCR_SWEN = 1;                  //  Enable switching.
    while (CLK_SWCR_SWBSY != 0);        //  Pause while the clock switch is busy.
}
void stm8_uart_print(char *message, unsigned char lenght)
{
    for (unsigned char  i = 0; i < lenght; i++)
    {
        stm8_uart_put_char(message[i]);     //  Put the next character into the data transmission function.
        while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
    }
}
void stm8_uart_printf(char *message)
{
    char *ch = message;
    while (*ch)
    {
        stm8_uart_put_char(*ch);     //  Put the next character into the data transmission function.
        while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
        ch++;                               //  Grab the next character.
    }
}
void stm8_uart_print_float(double f)
{
    int i = (int) (f*10);
    if (i < 0)
        stm8_uart_put_char('-');
    else
        stm8_uart_put_char(' ');
    i = abs(i);
    i = i%10000;
    stm8_uart_put_char(i/1000+'0');
    i = i%1000;
    stm8_uart_put_char(i/100+'0');
    i = i%100;
    stm8_uart_put_char(i/10+'0');
    stm8_uart_put_char('.');
    i = i %10;
    stm8_uart_put_char(i+'0');
}
void stm8_uart_print_int(int i)
{
    if (i < 0)
        stm8_uart_put_char('-');
    else
    stm8_uart_put_char(' ');
    i = abs(i);
    i = i%1000000;
    stm8_uart_put_char(i/100000+'0');
    i = i%100000;
    stm8_uart_put_char(i/10000+'0');
    i = i%10000;
    stm8_uart_put_char(i/1000+'0');
    i = i%1000;
    stm8_uart_put_char(i/100+'0');
    i = i%100;
    stm8_uart_put_char(i/10+'0');
    i = i %10;
    stm8_uart_put_char(i+'0');
}

void stm8_uart_print_u8(int i)
{
    if (i < 0)
        stm8_uart_put_char('-');
    else
    stm8_uart_put_char(' ');
    i = abs(i);
    // i = i%1000000;
    // stm8_uart_put_char(i/100000+'0');
    // i = i%100000;
    // stm8_uart_put_char(i/10000+'0');
    // i = i%10000;
    // stm8_uart_put_char(i/1000+'0');
    i = i%1000;
    stm8_uart_put_char(i/100+'0');
    i = i%100;
    stm8_uart_put_char(i/10+'0');
    i = i %10;
    stm8_uart_put_char(i+'0');
}

void stm8_uart_put_char(unsigned char c)
{
    UART1_DR = (unsigned char) c;     //  Put character into the data transmission register.
    while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
    if (c == '\r')
        UART1_DR = (unsigned char) '\n';     //  Put character into the data transmission register.
    if (c == '\n')
        UART1_DR = (unsigned char) '\r';     //  Put character into the data transmission register.
    while (UART1_SR_TXE == 0);          //  Wait for transmission to complete.
}
