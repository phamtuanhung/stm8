/* stm8_sonic.h */

#ifndef __STM8_SONIC_H__
#define __STM8_SONIC_H__

#include <stdint.h>
#include <iostm8s103f3.h>
#include "stm8_timer.h"

void stm8_sonic_init(void);
void stm8_sonic_triger(void);
float stm8_sonic_get_distance(void);

#endif
