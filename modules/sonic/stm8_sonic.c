/* stm8_sonic.c */

#include "stm8_sonic.h"
#include "stm8_gpio.h"
#include "stm8_uart.h"

void stm8_sonic_triger_end(void);
void stm8_sonic_echo(void);

uint16_t gDistance = 0;

void stm8_sonic_init(void)
{
    stm8_timer_set_compare(stm8_sonic_triger_end,10); // 10us
    stm8_gpio_exti_init();
    stm8_gpio_exti_set_callback(stm8_sonic_echo);
}

void stm8_sonic_triger(void)
{
    stm8_timer_reset_counter();
    stm8_gpio_set_triger(ON);
    stm8_timer_start_compare();
}

void stm8_sonic_triger_end(void)
{
    stm8_gpio_set_triger(OFF);
    stm8_timer_reset_counter();
}

void stm8_sonic_echo(void)
{
    if(stm8_gpio_get_echo() != 0)
    {
        stm8_timer_start_counter();
    }
    else
    {
        stm8_timer_stop_counter();
        gDistance = stm8_timer_counter_get_value();
        // stm8_uart_print_int(gDistance);
        // stm8_uart_printf("\r");
        stm8_timer_reset_counter();
    }
}

float stm8_sonic_get_distance(void)
{
    return ((float)gDistance)/10;
}

