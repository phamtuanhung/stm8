/* stm8_sensor.c */

#include "stm8_sensor.h"

static sensor_data_t sensorDataOffset;

static void delay (int time_ms) {
  volatile long int x;
  for (x = 0; x < 200*time_ms; ++x)
    x=x;
}

void stm8_sensor_init(void)
{
  stm8_i2c_init();
  stm8_sensor_mpu6050_init();
  stm8_sensor_mpu6050_calibrate();
}

void stm8_sensor_mpu6050_init(void)
{
  stm8_i2c_set_stop();
  delay(100);
  stm8_i2c_set_start_ack();
  stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
  stm8_i2c_send_reg(0x6B);
  stm8_i2c_send_reg(0x80);
  stm8_i2c_set_stop();
  delay(100);
  
  stm8_i2c_set_start_ack();
  stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
  stm8_i2c_send_reg(0x6B);
  stm8_i2c_send_reg(0x00);
  stm8_i2c_set_stop();
  delay(100);
}

void stm8_sensor_mpu6050_calibrate(void)
{
    stm8_i2c_set_start_ack();
    stm8_i2c_send_address (STM8_I2C_WRITE, MPU6050_ADDR);
    stm8_i2c_send_reg(0x1A);
    stm8_i2c_send_reg(0x04);
    stm8_i2c_set_stop();
    delay(100);
    
    sensor_data_t sensorData;
    sensor_data_t sumSensorData;
    sumSensorData.accel_x = 0;
    sumSensorData.accel_y = 0;
    int32_t sumSensorDataAccel_z = 0;
    
    sumSensorData.gyro_x = 0;
    sumSensorData.gyro_y = 0;
    sumSensorData.gyro_z = 0;
    
    for (int i = 0; i < 20; i++)
    {
        sensorData = stm8_sensor_get_value();
        delay(10);
    }
    
    for (int i = 0; i < 100; i++)
    {
        sensorData = stm8_sensor_get_value();
        sumSensorData.accel_x = sumSensorData.accel_x + sensorData.accel_x;
        sumSensorData.accel_y = sumSensorData.accel_y + sensorData.accel_y;
        sumSensorDataAccel_z = sumSensorDataAccel_z + sensorData.accel_z;
        
        sumSensorData.gyro_x = sumSensorData.gyro_x+sensorData.gyro_x;
        sumSensorData.gyro_y = sumSensorData.gyro_y+sensorData.gyro_y;
        sumSensorData.gyro_z = sumSensorData.gyro_z+sensorData.gyro_z;
        
        delay(10);
    }
    sensorDataOffset.accel_x = sumSensorData.accel_x / 100;
    sensorDataOffset.accel_y = sumSensorData.accel_y / 100;
    sensorDataOffset.accel_z = (sumSensorDataAccel_z / 100)  - 16384;
    
    sensorDataOffset.gyro_x  = sumSensorData.gyro_x / 100;
    sensorDataOffset.gyro_y  = sumSensorData.gyro_y / 100;
    sensorDataOffset.gyro_z  = sumSensorData.gyro_z / 100;
    
    // stm8_uart_printf("OFFSET ......... \r");
    // stm8_uart_print_int((int)(sensorDataOffset.accel_x));
    // stm8_uart_print_int((int)(sensorDataOffset.accel_y));
    // stm8_uart_print_int((int)(sensorDataOffset.accel_z));
    // stm8_uart_printf("\rDONE!!!!!!!!!!!\r");
    // sensorDataOffset = stm8_sensor_get_value();
    // sensorDataOffset.accel_x  = 0;
    // sensorDataOffset.accel_y  = 0;
    // sensorDataOffset.accel_z  = 0;
    //sensorDataOffset.accel_z = sensorDataOffset.accel_z - 2048;
}

sensor_data_t stm8_sensor_get_value(void){
  sensor_data_t sensorData;
  uint8_t xh,xl; 
  
  xh = stm8_i2c_read_register (ACCEL_XOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (ACCEL_XOUT_L, MPU6050_ADDR);
  sensorData.accel_x = xh << 8 | xl;
  xh = stm8_i2c_read_register (ACCEL_YOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (ACCEL_YOUT_L, MPU6050_ADDR);
  sensorData.accel_y = xh << 8 | xl;
  xh = stm8_i2c_read_register (ACCEL_ZOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (ACCEL_ZOUT_L, MPU6050_ADDR);
  sensorData.accel_z = xh << 8 | xl;
  
  xh = stm8_i2c_read_register (GYRO_XOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (GYRO_XOUT_L, MPU6050_ADDR);
  sensorData.gyro_x  = xh << 8 | xl;
  xh = stm8_i2c_read_register (GYRO_YOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (GYRO_YOUT_L, MPU6050_ADDR);
  sensorData.gyro_y  = xh << 8 | xl;
  xh = stm8_i2c_read_register (GYRO_ZOUT_H, MPU6050_ADDR);
  xl = stm8_i2c_read_register (GYRO_ZOUT_L, MPU6050_ADDR);
  sensorData.gyro_z  = xh << 8 | xl;
  
  sensorData.accel_x = sensorData.accel_x - sensorDataOffset.accel_x;
  sensorData.accel_y = sensorData.accel_y - sensorDataOffset.accel_y;
  sensorData.accel_z = sensorData.accel_z - sensorDataOffset.accel_z;
  
  sensorData.gyro_x = sensorData.gyro_x -sensorDataOffset.gyro_x;
  sensorData.gyro_y = sensorData.gyro_y -sensorDataOffset.gyro_y;
  sensorData.gyro_z = sensorData.gyro_z -sensorDataOffset.gyro_z;
  
  return(sensorData);
}