/* stm8_adc.c */

#include "stm8_adc.h"

void SetupADC()
{
    ADC_CSR_CH = 0x04;      //  ADC on AIN4 only.
    ADC_CR3_DBUF = 0;
    ADC_CR2_ALIGN = 1;      //  Data is right aligned.
    ADC_CSR_EOCIE = 1;      //  Enable the interrupt after conversion completed.
}

unsigned int stm8_adc_read_channel(unsigned int channel) 
{
    unsigned int val=0;
    //using ADC in single conversion mode
    ADC_CSR_CH = ((0x0F)&channel); // select channel
    ADC_CR2_ALIGN = 1; // Right Aligned Data
    ADC_CR1_ADON = 1; // ADC ON 
    ADC_CR1_ADON = 1; // ADC Start Conversion
    while(ADC_CSR_EOC== 0); // Wait till EOC
    val |= (unsigned int)ADC_DRL;
    val |= (unsigned int)ADC_DRH<<8;
    ADC_CR1_ADON = 0; // ADC Stop Conversion
    ADC_CSR_EOC = 0;        //     Indicate that ADC conversion is complete.
    val &= 0x03ff;
    return (val);
}

void StartADC()
{
    ADC_CR1_ADON = 1;       //  Second write starts the conversion.
}
#pragma vector = ADC1_EOC_vector
__interrupt void ADC1_EOC_IRQHandler()
{
    unsigned char low, high;
    int reading;

    ADC_CR1_ADON = 0;       //  Disable the ADC.
    ADC_CSR_EOC = 0;        //     Indicate that ADC conversion is complete.

    low = ADC_DRL;            //    Extract the ADC reading.
    high = ADC_DRH;
    //
    //  Calculate the values for the capture compare register and restart Timer 1.
    //
    reading = 1023 - ((high * 256) + low);
    low = reading & 0xff;
    high = (reading >> 8) & 0xff;
}