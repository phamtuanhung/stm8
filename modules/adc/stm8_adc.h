/* stm8_adc.h */

#include <stdint.h>
#include <iostm8s103f3.h>

unsigned int stm8_adc_read_channel(unsigned int channel);
