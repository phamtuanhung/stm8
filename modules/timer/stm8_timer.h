/* stm8_timer.h */

#ifndef __STM8_TIMER_H__
#define __STM8_TIMER_H__

#include <stdint.h>
#include <iostm8s103f3.h>

typedef void (*stm8_timer_interval_callback_function_t)(void);
typedef void (*stm8_timer_compare_callback_function_t)(void);

void stm8_timer_delay(uint16_t nCount);
void stm8_timer_init(void);
void stm8_timer_interval_init(void);
void stm8_timer_interval_enable();
void stm8_timer_interval_disable();
void stm8_timer_set_interval(stm8_timer_interval_callback_function_t function, uint16_t ms);
void stm8_timer_set_compare(stm8_timer_compare_callback_function_t function, uint16_t us);
void stm8_timer_interval_default_function(void);
void stm8_timer_counter_init(void);
uint16_t stm8_timer_couter_get_loop(void);
uint16_t stm8_timer_counter_get_value(void);
void stm8_timer_start_compare(void);
void stm8_timer_start_counter(void);
void stm8_timer_stop_counter(void);
void stm8_timer_reset_counter(void);

#endif
