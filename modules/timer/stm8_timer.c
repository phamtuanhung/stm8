/* stm8_timer.c */

#include "stm8_timer.h"
#include "stm8_gpio.h"

void stm8_timer_default_callback_function(void);
stm8_timer_interval_callback_function_t pIntervalCallbackPointer;
stm8_timer_compare_callback_function_t pCompareCallbackPointer;
uint16_t gIntervalValue = 10;
uint16_t gIntervalCounter = 0;
uint16_t gLastCounterValue;

void stm8_timer_delay(uint16_t ms)
{
    /* Decrement ms value */
    while (ms != 0)
    {
        ms--;
        uint16_t count = 1000;
        while (count != 0)
        {
            count--;
        }
    }
}
void stm8_timer_init(void)
{
    // Init interval timer
    stm8_timer_interval_init();
    pIntervalCallbackPointer = stm8_timer_default_callback_function;
    pCompareCallbackPointer = stm8_timer_default_callback_function;
    
    // Init counter timer
    stm8_timer_counter_init();
    gLastCounterValue = stm8_timer_counter_get_value();
}

void stm8_timer_interval_init(void)
{
    /* Set the Autoreload value */
    TIM1_ARRH = (uint8_t)(1000 >> 8);
    TIM1_ARRL = (uint8_t)(1000);  
    /* Set the Prescaler value */
    TIM1_PSCRH = (uint8_t)(0 >> 8);
    TIM1_PSCRL = (uint8_t)(13);  
    /* Set the Repetition Counter value */
    TIM1_RCR = 0; 
    
    TIM1_IER |= (uint8_t)0x01;
    
    /*TIM1 counter enable */
    TIM1_CR1 |= 0x01;
}

void stm8_timer_interval_enable()
{
  TIM1_IER |= (uint8_t)0x01;
}
void stm8_timer_interval_disable()
{
  TIM1_IER &= (uint8_t)(~0x01);
}

void stm8_timer_default_callback_function(void)
{
    
}

void stm8_timer_set_interval(stm8_timer_interval_callback_function_t function, uint16_t ms)
{
    pIntervalCallbackPointer = function;
    gIntervalValue = ms;
}

void stm8_timer_set_compare(stm8_timer_compare_callback_function_t function, uint16_t us)
{
    pCompareCallbackPointer = function;
    TIM2_CCR2H = (uint8_t)(us>>8);       //  High byte
    TIM2_CCR2L = (uint8_t)(us&0x00FF);   //  Low byte
}

#pragma vector = 13
__interrupt void TIM1_UPD_OVF_TRG_BRK_IRQHandler(void)
{
    TIM1_SR1 = (uint8_t)(~0x01);
    gIntervalCounter++;
    if ((gIntervalCounter==gIntervalValue))
    {
        (*pIntervalCallbackPointer)();
        gIntervalCounter = 0;
    }
}

void stm8_timer_counter_init(void)
{
    TIM2_CR1 = 0;               // Turn everything TIM2 related off.
    TIM2_IER = 0;
    TIM2_SR2 = 0;
    TIM2_CCER1 = 0;
    TIM2_CCER2 = 0;
    TIM2_CCER1 = 0;
    TIM2_CCER2 = 0;
    TIM2_CCMR1 = 0;
    TIM2_CCMR2 = 0;
    TIM2_CCMR3 = 0;
    TIM2_CNTRH = 0;
    TIM2_CNTRL = 0;
    TIM2_PSCR = 0;
    TIM2_ARRH  = 0;
    TIM2_ARRL  = 0;
    TIM2_CCR1H = 0;
    TIM2_CCR1L = 0;
    TIM2_CCR2H = 0;
    TIM2_CCR2L = 0;
    TIM2_CCR3H = 0;
    TIM2_CCR3L = 0;
    TIM2_SR1 = 0;
    
    TIM2_PSCR = 0x05;       //  Prescaler = 16.
    TIM2_ARRH = (uint8_t)(50000>>8);       //  High byte of 50,000.
    TIM2_ARRL = (uint8_t)(50000&0x00FF);   //  Low byte of 50,000.
    TIM2_CCMR2_OC2M = 0x02;
    TIM2_IER_UIE = 1;
    TIM2_PSCR = (uint8_t)0x07;//(TIM2_PRESCALER_16);
    /*TIM2 counter enable */
    TIM2_CR1 = (uint8_t)0x01;
}

void stm8_timer_reset_counter(void)
{
    TIM2_CNTRH = 0;
    TIM2_CNTRL = 0;
}

void stm8_timer_start_compare(void)
{
    TIM2_IER_CC2IE = 1;     //  Enable the compare interrupts.
    TIM2_CR1_CEN = 1;       //  enable the timer.
}

void stm8_timer_start_counter(void)
{
    TIM2_CR1_CEN = 1;       //  enable the timer.
}

void stm8_timer_stop_counter(void)
{
    TIM2_CR1_CEN = 0;
}

uint16_t t = 0;

#pragma vector = TIM2_OVR_UIF_vector
__interrupt void TIM2_UPD_OVF_IRQHandler(void)
{
    TIM2_SR1_UIF = 0;
    TIM2_CR1_CEN = 0;
}

#pragma vector = TIM2_CAPCOM_CC2IF_vector
__interrupt void TIM2_CAPCOM_CC2IF_IRQHandler(void)
{
    TIM2_SR1_CC2IF = 0;
    TIM2_IER_CC2IE = 0;     //  disable the compare interrupts.
    (*pCompareCallbackPointer)();
}

uint16_t stm8_timer_couter_get_loop(void)
{
    uint16_t currentValue = stm8_timer_counter_get_value();
    uint16_t diferenceValue = currentValue - gLastCounterValue;
    gLastCounterValue=currentValue;
    return diferenceValue;
}

uint16_t stm8_timer_counter_get_value(void)
{
    uint16_t tmpcntr = 0;  
    tmpcntr =  ((uint16_t)TIM2_CNTRH << 8);
    /* Get the Counter Register value */
    return (uint16_t)( tmpcntr| (uint16_t)(TIM2_CNTRL));
}
