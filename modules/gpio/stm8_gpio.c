/* stm8_gpio.c */

#include "stm8_gpio.h"

void stm8_gpio_init()
{
    stm8_gpio_led_init();
    stm8_gpio_exti_init();
}

void stm8_gpio_led_init(void)
{
    PD_ODR = 0; //Turn off all pins
    PD_DDR_DDR1 = 0; //PortD, Bit 1 is input
    PD_DDR_DDR2 = 1; //PortD, Bit 2 is output
    PD_DDR_DDR3 = 0; //PortD, Bit 3 is input
    PA_DDR_DDR3 = 1; //PortA, Bit 3 is output
    PD_CR1_C11  = 1; //PortD, Bit 1 is pull-up.
    PD_CR1_C13  = 1; //PortD, Bit 3 is pull-up.
    PA_CR1_C13  = 1; //PortA, Bit 3 is pull-up.
}

void stm8_gpio_switch_init(void)
{
    PA_ODR_ODR2 = 0;             //  All pins are turned off.
    PA_CR2_C22 = 0;          //  Output speeds up to 10 MHz.
  
    PA_DDR_DDR2 = 0;        //  PA2 is input.
    PA_CR1_C12 = 0;         //  PA2 is floating input.
    PA_CR1_C12 = 0;         //  PA2 is floating input.
}
uint8_t stm8_gpio_get_switch(void)
{
    return PA_IDR&STM8_GPIO_SWITCH_PIN;
}

uint8_t stm8_gpio_get_echo(void)
{
    return PA_IDR_IDR1;
}

void stm8_gpio_set_led(uint8_t state)
{
  if (state == ON)
    PD_ODR &= (uint8_t)(~STM8_GPIO_LED_PIN);
  else
    PD_ODR |= (uint8_t)STM8_GPIO_LED_PIN;
}

void stm8_gpio_set_triger(uint8_t state)
{
  if (state == ON)
    PA_ODR &= (uint8_t)(~STM8_GPIO_TRIGER_PIN);
  else
    PA_ODR |= (uint8_t)STM8_GPIO_TRIGER_PIN;
}
void stm8_gpio_toggle_led(void)
{
  PD_ODR ^= (uint8_t)STM8_GPIO_LED_PIN;
}

stm8_gpio_exti_callback_function_t extiCallbackPointer;

void stm8_gpio_exti_init(void)
{
    PA_ODR = 0;             //  All pins are turned off.
    PA_CR2 = 0xff;          //  Output speeds up to 10 MHz.
  
    PA_DDR_DDR2 = 0;        //  PA2 is input.
    PA_DDR_DDR1 = 0;        //  PA2 is input.
    PA_CR1_C12 = 0;         //  PA2 is floating input.
    PA_CR1_C11 = 0;         //  PA2 is floating input.
    EXTI_CR1_PAIS = 3;      //  Interrupt on falling and rising edge.
    
    extiCallbackPointer = stm8_gpio_exti_dump_fuction;
}

#pragma vector = 5
__interrupt void EXTI_PORTA_IRQHandler(void)
{
    (*extiCallbackPointer)();
}
void stm8_gpio_exti_set_callback(stm8_gpio_exti_callback_function_t function)
{
    extiCallbackPointer = function;
}
void stm8_gpio_exti_dump_fuction()
{
    
}