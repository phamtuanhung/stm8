/* stm8_gpio.h */

#include <stdint.h>
#include <iostm8s103f3.h>

#define STM8_GPIO_LED_PIN     (1<<2)   //(D2)
#define STM8_GPIO_SWITCH_PIN  (1<<2)   //(A2)
#define STM8_GPIO_TRIGER_PIN  (1<<3)   //(A1)
#define ON 1
#define OFF 0

typedef void (*stm8_gpio_exti_callback_function_t)(void);

void stm8_gpio_init();
void stm8_gpio_led_init(void);
void stm8_gpio_set_led(uint8_t state);
void stm8_gpio_set_triger(uint8_t state);
void stm8_gpio_toggle_led(void);
void stm8_gpio_exti_init(void);
void stm8_gpio_exti_set_callback(stm8_gpio_exti_callback_function_t function);
void stm8_gpio_exti_dump_fuction();
void stm8_gpio_switch_init(void);
uint8_t stm8_gpio_get_switch(void);
uint8_t stm8_gpio_get_echo(void);

